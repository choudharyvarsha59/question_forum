"""create first script

Revision ID: a11ea0d29995
Revises: 
Create Date: 2017-02-25 12:29:49.350067

"""
from alembic import op
import sqlalchemy as sa
import datetime


# revision identifiers, used by Alembic.
revision = 'a11ea0d29995'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
        sa.Column('created_at', sa.DateTime,default=datetime.datetime.utcnow)
        )

    op.create_table(
        'question',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('title', sa.String(50), nullable=False),
        sa.Column('description', sa.String(100), nullable=False),
        sa.Column('created_at', sa.DateTime,default=datetime.datetime.utcnow)
        )

    op.create_table(
        'answer',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('question_id',sa.Integer,nullable=False),
        sa.Column('answer', sa.String(100), nullable=False),
        sa.Column('created_at', sa.DateTime,default=datetime.datetime.utcnow)

        )
    op.create_table(
        'question_comment',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('question_id',sa.Integer,nullable=False),
        sa.Column('comment', sa.String(100), nullable=False),
        sa.Column('created_at', sa.DateTime,default=datetime.datetime.utcnow)

        )
    op.create_table(
        'answer_comment',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('answer_id',sa.Integer,nullable=False),
        sa.Column('comment', sa.String(100), nullable=False),
        sa.Column('created_at', sa.DateTime,default=datetime.datetime.utcnow)
        )
    op.create_table(
        'question_like_dislike',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('question_id',sa.Integer,nullable=False),
        sa.Column('types', sa.Integer, nullable=False),

        )
    op.create_table(
        'answer_like_dislike',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('answer_id',sa.Integer,nullable=False),
        sa.Column('types', sa.Integer, nullable=False),

        )
    op.create_table(
        'question_comment_like_dislike',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('comment_id',sa.Integer,nullable=False),
        sa.Column('types', sa.Integer, nullable=False),

        )
    op.create_table(
        'answer_comment_like_dislike',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id',sa.Integer,nullable=False),
        sa.Column('comment_id',sa.Integer,nullable=False),
        sa.Column('types', sa.Integer, nullable=False),

        )
    op.create_foreign_key('answer_key', 'question','user', ['user_id'],['id'])
    op.create_foreign_key('answer_user_key', 'answer','user', ['user_id'],['id'])
    op.create_foreign_key('answer_question_key', 'answer','question', ['question_id'],['id'])
    op.create_foreign_key('question_comment_user_key', 'question_comment','user', ['user_id'],['id'])
    op.create_foreign_key('question_comment_question_key', 'question_comment','question', ['question_id'],['id'])
    op.create_foreign_key('answer_comment_user_key', 'answer_comment','user', ['user_id'],['id'])
    op.create_foreign_key('answer_comment_answer__key', 'answer_comment','answer', ['answer_id'],['id'])
    op.create_foreign_key('question_user_like/dislike_key', 'question_like_dislike','user', ['user_id'],['id'])
    op.create_foreign_key('question_like_dislike_key', 'question_like_dislike','question', ['question_id'],['id'])
    op.create_foreign_key('anser_user_like_dislike_key', 'answer_like_dislike','user', ['user_id'],['id'])
    op.create_foreign_key('answer_like_dislike_key', 'answer_like_dislike','answer', ['answer_id'],['id'])
    op.create_foreign_key('questcom_like_dislike_key', 'question_comment_like_dislike','user', ['user_id'],['id'])
    op.create_foreign_key('commt_like_dislike_key', 'question_comment_like_dislike','question_comment', ['comment_id'],['id'])
    op.create_foreign_key('anscom_like_dislike_key', 'answer_comment_like_dislike','user', ['user_id'],['id'])
    op.create_foreign_key('com_like_dislike_key', 'answer_comment_like_dislike','answer_comment', ['comment_id'],['id'])





def downgrade():
    op.drop_constraint('answer_key', 'question', type_='foreignkey')
    op.drop_constraint('answer_user_key', 'answer', type_='foreignkey')
    op.drop_constraint('answer_question_key', 'answer', type_='foreignkey')
    op.drop_constraint('question_comment_user_key', 'question_comment', type_='foreignkey')
    op.drop_constraint('question_comment_question_key', 'question_comment', type_='foreignkey')
    op.drop_constraint('answer_comment_user_key', 'answer_comment', type_='foreignkey')
    op.drop_constraint('answer_comment_answer__key', 'answer_comment', type_='foreignkey')
    op.drop_constraint('question_user_like_dislike_key', 'Question_Like_Dislike', type_='foreignkey')
    op.drop_constraint('question_like_dislike_key', 'Question_Like_Dislike', type_='foreignkey')
    op.drop_constraint('anser_user_like_dislike_key', 'Answer_Like_Dislike', type_='foreignkey')
    op.drop_constraint('answer_like_dislike_key', 'Answer_Like_Dislike', type_='foreignkey')
    op.drop_constraint('questcom_like_dislike_key', 'question_comment_like_dislike', type_='foreignkey')
    op.drop_constraint('commt_like_dislike_key', 'question_comment_like_dislike', type_='foreignkey')
    op.drop_constraint('anscom_like_dislike_key', 'answer_comment_like_dislike', type_='foreignkey')
    op.drop_constraint('com_like/dislike_key', 'answer_comment_like_dislike', type_='foreignkey')
    op.drop_table('user')
    op.drop_table('question')
    op.drop_table('answer')
    op.drop_table('question_comment')
    op.drop_table('answer_comment')
    op.drop_table('question_like_dislike')
    op.drop_table('answer_like_dislike')
    op.drop_table('comment_like_dislike')







