"""Add columns

Revision ID: 9920d634a2a3
Revises: a11ea0d29995
Create Date: 2017-02-25 14:05:38.184505

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9920d634a2a3'
down_revision = 'a11ea0d29995'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
