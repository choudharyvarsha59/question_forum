from app import app
from app.models.table import Question,Question_Comment,Question_Like_Dislike,question_Comment_Like_Dislike
from flask import request,jsonify,abort

@app.route('/question/add',methods=['POST'])
def add_question():
    request_data=request.json
    user_id=request_data.get('user_id')
    title=request_data.get('title')
    description=request_data.get('description')
    if not user_id:
        abort(400,"user_id is missing")
    if not title:
        abort(400,"title is missing")   
    if not description:
        abort(400,"description is missing")   
    question=Question()
    id=question.add_new_question(request_data) 
    result={'id':id}
    return jsonify(result)   

@app.route('/question/list',methods=['GET'])
def question_list():
    q_list=Question()
    questions=q_list.get_all_questions()
    result=[]
    for question in questions:
        data=question.serialize()
        result.append(data)
    return jsonify(result)  

@app.route('/question/comment',methods=['POST'])
def add_question_comment():
    request_data=request.json
    user_id=request_data.get('user_id')
    question_id=request_data.get('question_id')
    comment=request_data.get('comment')
    if not user_id:
        abort(400,"user_id is missing")
    if not question_id:
        abort(400,"question_id is missing")    
    if not comment:
        abort(400,"comment is missing")  
    ans=Question_Comment()
    id=ans.add_new_question_comment(request_data)  
    result={'id':id}
    return jsonify(result)   

@app.route('/question/like',methods=['POST'])
def add_question_like():
    request_data=request.json
    user_id=request_data.get('user_id')
    question_id=request_data.get('question_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not question_id:
        abort(400,"question_id is missing")  
    ans=Question_Like_Dislike()
    res=ans.add_like(request_data,1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result)    

@app.route('/question/dislike',methods=['POST'])
def add_question_dislike():
    request_data=request.json
    user_id=request_data.get('user_id')
    question_id=request_data.get('question_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not question_id:
        abort(400,"question_id is missing")  
    ans=Question_Like_Dislike()
    res=ans.add_dislike(request_data,-1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result) 

@app.route('/question/comment/like',methods=['POST'])
def add_question_comment_like():
    request_data=request.json
    user_id=request_data.get('user_id')
    question_comment_id=request_data.get('question_comment_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not question_comment_id:
        abort(400,"question_comment_id is missing")  
    ans=question_Comment_Like_Dislike()
    res=ans.add_like(request_data,1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result)    

@app.route('/question/comment/dislike',methods=['POST'])
def add_question_comment_dislike():
    request_data=request.json
    user_id=request_data.get('user_id')
    question_comment_id=request_data.get('question_comment_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not question_comment_id:
        abort(400,"question_comment_id is missing")  
    ans=question_Comment_Like_Dislike()
    res=ans.add_dislike(request_data,-1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result)

@app.route('/question/details/<id>',methods=['GET'])
def question_details(id):
    question=Question()
    result=question.question_detail(id)
    return jsonify(result)

   




