from app import app
from flask import request,abort,jsonify,render_template
from app.models.table  import User



@app.route('/user/add',methods=['POST'])
def add_user():
    request_data=request.json
    name=request_data.get('name')
    if not name:
        abort(400,"name is missing")
    user=User() 
    id=user.add_new_user(request_data)
    result={'id':id}
    return jsonify(result) 

@app.route('/',methods=['GET'])
def home():
    return render_template("base.html")
