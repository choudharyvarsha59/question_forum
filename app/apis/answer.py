from app import app
from app.models.table import Answer,Answer_Comment,Answer_Like_Dislike,answer_Comment_Like_Dislike
from flask import request,jsonify,abort

@app.route('/answer/add',methods=['POST'])
def add_answer():
    request_data=request.json
    user_id=request_data.get('user_id')
    question_id=request_data.get('question_id')
    answer=request_data.get('answer')
    if not user_id:
        abort(400,"user_id is missing")
    if not question_id:
        abort(400,"question_id is missing")    
    if not answer:
        abort(400,"answer is missing")  
    answer=Answer()
    id=answer.add_new_answer(request_data)  
    result={'id':id}
    return jsonify(result)   

@app.route('/answer/comment',methods=['POST'])
def add_answer_comment():
    request_data=request.json
    user_id=request_data.get('user_id')
    answer_id=request_data.get('answer_id')
    comment=request_data.get('comment')
    if not user_id:
        abort(400,"user_id is missing")
    if not answer_id:
        abort(400,"answer_id is missing")    
    if not comment:
        abort(400,"comment is missing")  
    ans=Answer_Comment()
    id=ans.add_new_answer_comment(request_data)  
    result={'id':id}
    return jsonify(result)   

@app.route('/answer/like',methods=['POST'])
def add_anser_like():
    request_data=request.json
    user_id=request_data.get('user_id')
    answer_id=request_data.get('answer_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not answer_id:
        abort(400,"answer_id is missing")  
    ans=Answer_Like_Dislike()
    res=ans.add_like(request_data,1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result)    

@app.route('/answer/dislike',methods=['POST'])
def add_anser_dislike():
    request_data=request.json
    user_id=request_data.get('user_id')
    answer_id=request_data.get('answer_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not answer_id:
        abort(400,"answer_id is missing")  
    ans=Answer_Like_Dislike()
    res=ans.add_dislike(request_data,-1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result)        

@app.route('/answer/comment/like',methods=['POST'])
def add_answer_comment_like():
    request_data=request.json
    user_id=request_data.get('user_id')
    answer_comment_id=request_data.get('answer_comment_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not answer_comment_id:
        abort(400,"answer_comment_id is missing")  
    ans=answer_Comment_Like_Dislike()
    res=ans.add_like(request_data,1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result)    

@app.route('/answer/comment/dislike',methods=['POST'])
def add_answer_comment_dislike():
    request_data=request.json
    user_id=request_data.get('user_id')
    answer_comment_id=request_data.get('answer_comment_id')
    if not user_id:
        abort(400,"user_id is missing")
    if not answer_comment_id:
        abort(400,"answer_comment_id is missing")  
    ans=answer_Comment_Like_Dislike()
    res=ans.add_dislike(request_data,-1)  
    if res:
        result={"status":True}
    else:
        result={"status":False}
    return jsonify(result) 

@app.route('/question/answers/<id>',methods=['GET'])
def question_answers(id):
    request_data = request.args
    page = int(request_data.get('page', 0))
    per_page = int(request_data.get('per_page', 2))
    answers= Answer()
    answers=answers.get_all_answers(id, page, per_page)  
    return jsonify(answers)
