from flask import Flask
from flask_sqlalchemy import SQLAlchemy
app=Flask(__name__)
app.config.from_object('app.settings')
 
db=SQLAlchemy(app)

__import__('app.apis.user')
__import__('app.apis.question')
__import__('app.apis.answer')


