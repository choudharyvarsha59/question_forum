from app import db
import datetime

class User(db.Model):
    __tablename__='user'

    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(50),nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    questions=db.relationship('Question',backref='user_detail',lazy="dynamic")
    question_comments=db.relationship('Question_Comment',backref='user_detail',lazy="dynamic")
    ansers=db.relationship('Answer',backref='user_detail',lazy="dynamic")
    answer_comments=db.relationship('Answer_Comment',backref='user_detail',lazy="dynamic")

    def add_new_user(self,data):
        try:
            user=User()
            user.name=data['name']
            db.session.add(user)
            db.session.flush()
            db.session.commit()
            return user.id
        except Exception as e:
            db.session.rollback()
            print e

    def serialize(self):
           data={}
           data['id']=self.id
           data['name']=self.name
           data['created_at']=self.created_at
           return data
 




 

class Question(db.Model):
    __tablename__='question'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    title=db.Column(db.String(50),nullable=False)
    description=db.Column(db.String(100),nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    comments=db.relationship('Question_Comment',backref='its_question',lazy="dynamic")


    def add_new_question(self,data):
        try:
            question=Question()
            question.user_id=data['user_id']
            question.title=data['title']
            question.description=data['description']
            db.session.add(question)
            db.session.flush()
            db.session.commit()
            return question.id
        except Exception as e:
            db.session.rollback()
            print e

    def serialize(self):
           data={}
           data['id']=self.id
           data['user_id']=self.user_id
           data['title']=self.title
           data['description']=self.description
           data['created_at']=self.created_at
           return data


    def get_all_questions(self):
        questions=db.session.query(Question).all()
        return questions

    def get_comments(self):
            question=db.session.query(Question).filter_by(id=self.id).first()
            comments=question.comments
            comment_detail=[]
            for comment in comments:
                
                detail=comment.user_detail.serialize()
                comment=comment.serialize()
                obj=question_Comment_Like_Dislike()
                likes,dislikes=obj.total_like(comment['id'])
                comment['likes']=likes
                comment['dislikes']=dislikes
                comment['user']=detail
                comment_detail.append(comment)
            return comment_detail    


    def question_detail(self,q_id):
            question=db.session.query(Question).filter_by(id=q_id).first()
            data={}
            data['id']=question.id
            data['user_id']=question.user_id
            data['title']=question.title
            data['description']=question.description
            data['created_at']=question.created_at
            like_dislike_obj=Question_Like_Dislike()
            likes,dislikes=like_dislike_obj.total_like(q_id)
            data['likes']=likes
            data['dislikes']=dislikes
            detail=question.user_detail.serialize()
            data['user']=detail
            comments=question.get_comments()
            data['comments']=comments
            return data
            

            





                

class Answer(db.Model):
    __tablename__='answer'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    question_id=db.Column(db.Integer,db.ForeignKey('question.id'))
    answer=db.Column(db.String(100),nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    comments=db.relationship('Answer_Comment',backref='the_answer',lazy="dynamic")

    def serialize(self):
        data={}
        data['id']=self.id
        data['user_id']=self.user_id
        data['question_id']=self.question_id
        data['amswer']=self.answer
        data['created_at']=self.created_at
        return data

    def get_comments(self):
        answer=db.session.query(Answer).filter_by(id=self.id).first()
        comments=answer.comments
        comment_detail=[]
        for comment in comments:
            
            detail=comment.user_detail.serialize()
            comment=comment.serialize()
            comment['user']=detail
            obj=question_Comment_Like_Dislike()
            likes,dislikes=obj.total_like(comment['id'])
            comment['likes']=likes
            comment['dislikes']=dislikes
            comment_detail.append(comment)
        return comment_detail    

            

    def get_all_answers(self,id,page,per_page):
        all_answers=db.session.query(Answer).filter_by(question_id=id).limit(per_page).offset(page*per_page).all()
        answers=[]
        for answer in all_answers:
            likes_dislike=Answer_Like_Dislike()

            likes,dislikes=likes_dislike.total_like(answer.id)
            data=answer.serialize()
            detail=answer.user_detail.serialize()
            data['user']=detail
                        
            data['dislikes']=dislikes
            data['likes']=likes
            comments=answer.get_comments()
            data['comments']=comments
            

            answers.append(data)

        return answers



    def add_new_answer(self,data):
        try:
            ans=Answer()
            ans.user_id=data['user_id']
            ans.question_id=data['question_id']
            ans.answer=data['answer']
            db.session.add(ans)
            db.session.flush()
            db.session.commit()
            return ans.id
        except Exception as e:
            db.session.rollback()
            print e

            

class Question_Comment(db.Model):
    __tablename__='question_comment'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    question_id=db.Column(db.Integer,db.ForeignKey('question.id'))
    comment=db.Column(db.String(100),nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def serialize(self):
           data={}
           data['id']=self.id
           data['user_id']=self.user_id
           data['question_id']=self.question_id
           data['comment']=self.comment
           data['created_at']=self.created_at
           return data


    def add_new_question_comment(self,data):
        try:
            ans=Question_Comment()
            ans.user_id=data['user_id']
            ans.question_id=data['question_id']
            ans.comment=data['comment']
            db.session.add(ans)
            db.session.flush()
            db.session.commit()
            return ans.id
        except Exception as e:
            db.session.rollback()
            print e


class Answer_Comment(db.Model):
    __tablename__='answer_comment'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    answer_id=db.Column(db.Integer,db.ForeignKey('answer.id'))
    comment=db.Column(db.String(100),nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def serialize(self):
           data={}
           data['id']=self.id
           data['user_id']=self.user_id
           data['answer_id']=self.answer_id
           data['comment']=self.comment
           data['created_at']=self.created_at
           return data

    def add_new_answer_comment(self,data):
        try:
            ans=Answer_Comment()
            ans.user_id=data['user_id']
            ans.answer_id=data['answer_id']
            ans.comment=data['comment']
            db.session.add(ans)
            db.session.flush()
            db.session.commit()
            return ans.id
        except Exception as e:
            db.session.rollback()
            print e



class Question_Like_Dislike(db.Model):
    __tablename__='question_like_dislike'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    question_id=db.Column(db.Integer,db.ForeignKey('question.id'))
    types=db.Column(db.Integer,nullable=False)

    def total_like(self,q_id):
        likes_dislike=db.session.query(Question_Like_Dislike.types, db.func.count(id)).filter_by(question_id=q_id).group_by(Question_Like_Dislike.types).all()
        likes,dislikes=0,0
        for like in range(len(likes_dislike)):
            if likes_dislike[like]:
                if likes_dislike[like][0]==-1:
                    dislikes=likes_dislike[like][1]
                if likes_dislike[like][0]==1:
                    likes=likes_dislike[like][1]    
             
        return likes,dislikes
                

    def add_like(self,data,value):
        try:
            obj=Question_Like_Dislike()
            obj.user_id=data['user_id']
            obj.question_id=data['question_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    

    def add_dislike(self,data,value):
        try:
            obj=Question_Like_Dislike()
            obj.user_id=data['user_id']
            obj.question_id=data['question_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    

class Answer_Like_Dislike(db.Model):
    __tablename__='answer_like_dislike'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    answer_id=db.Column(db.Integer,db.ForeignKey('answer.id'))
    types=db.Column(db.Integer,nullable=False)

    def add_like(self,data,value):
        try:
            obj=Answer_Like_Dislike()
            obj.user_id=data['user_id']
            obj.answer_id=data['answer_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    

    def add_dislike(self,data,value):
        try:
            obj=Answer_Like_Dislike()
            obj.user_id=data['user_id']
            obj.answer_id=data['answer_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    

    def total_like(self,a_id):
        likes_dislike=db.session.query(Answer_Like_Dislike.types, db.func.count(id)).filter_by(answer_id=a_id).group_by(Answer_Like_Dislike.types).all()
        likes,dislikes=0,0
        for like in range(len(likes_dislike)):
            if likes_dislike[like]:
                if likes_dislike[like][0]==-1:
                    dislikes=likes_dislike[like][1]
                if likes_dislike[like][0]==1:
                    likes=likes_dislike[like][1]    
               
        return likes,dislikes        

class question_Comment_Like_Dislike(db.Model):
    __tablename__='question_comment_like_dislike'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    comment_id=db.Column(db.Integer,db.ForeignKey('question_comment.id'))
    types=db.Column(db.Integer,nullable=False)


    def total_like(self,id):
        likes_dislike=db.session.query(question_Comment_Like_Dislike.types, db.func.count(id)).filter_by(comment_id=id).group_by(question_Comment_Like_Dislike.types).all()
        likes,dislikes=0,0
        for like in range(len(likes_dislike)):
            if likes_dislike[like]:
                if likes_dislike[like][0]==-1:
                    dislikes=likes_dislike[like][1]
                if likes_dislike[like][0]==1:
                    likes=likes_dislike[like][1]             
        return likes,dislikes

    def add_like(self,data,value):
        try:
            obj=question_Comment_Like_Dislike()
            obj.user_id=data['user_id']
            obj.comment_id=data['question_comment_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    

    def add_dislike(self,data,value):
        try:
            obj=question_Comment_Like_Dislike()
            obj.user_id=data['user_id']
            obj.comment_id=data['question_comment_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    
    
class answer_Comment_Like_Dislike(db.Model):
    __tablename__='answer_comment_like_dislike'

    id=db.Column(db.Integer,primary_key=True)
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    comment_id=db.Column(db.Integer,db.ForeignKey('answer_comment.id'))
    types=db.Column(db.Integer,nullable=False)

    def total_like(self,q_id):
        likes_dislike=db.session.query(answer_Comment_Like_Dislike.types, db.func.count(id)).filter_by(comment_id=q_id).group_by(answer_Comment_Like_Dislike.types).all()
        likes,dislikes=0,0
        for like in range(len(likes_dislike)):
            if likes_dislike[like]:
                if likes_dislike[like][0]==-1:
                    dislikes=likes_dislike[like][1]
                if likes_dislike[like][0]==1:
                    likes=likes_dislike[like][1]    
               
        return likes,dislikes
                

    def add_like(self,data,value):
        try:
            obj=answer_Comment_Like_Dislike()
            obj.user_id=data['user_id']
            obj.comment_id=data['answer_comment_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    

    def add_dislike(self,data,value):
        try:
            obj=answer_Comment_Like_Dislike()
            obj.user_id=data['user_id']
            obj.comment_id=data['answer_comment_id']
            obj.types=value
            db.session.add(obj)
            db.session.flush()
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            print e    

